from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class PokedexCreature(models.Model):
    creation_date = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET(get_sentinel_user),
                                   related_name='pokedex_creature_created_by')
    name = models.CharField(blank=True, null=True, max_length=100)
    type1 = models.CharField(blank=True, null=True, max_length=100)
    type2 = models.CharField(blank=True, null=True, max_length=100)
    total = models.IntegerField(blank=True, null=True)
    hp = models.IntegerField(blank=True, null=True)
    attack = models.IntegerField(blank=True, null=True)
    defense = models.IntegerField(blank=True, null=True)
    sp_atk = models.IntegerField(blank=True, null=True)
    sp_def = models.IntegerField(blank=True, null=True)
    speed = models.IntegerField(blank=True, null=True)
    generation = models.IntegerField(blank=True, null=True)
    legendary = models.BooleanField(default=False)

    def __str__(self):
        return self.name
