from rest_framework import status

from pokedex.models import PokedexCreature
from pokedex.tests.AbstractTestCase import AbstractTestCase
from pokedex.tests.generics import create_pokedex_creature


class CrudTests(AbstractTestCase):

    def setUp(self):
        super().setUp()
        for index in range(50):
            create_pokedex_creature(self.admin, f'pokedex_creature_test{index}')
        self.pokedex_creature = PokedexCreature.objects.first()
        self.pokedex_creature.legendary = True
        self.pokedex_creature.save()

    # Get all pokedex_creatures
    def test_get_all_pokedex_creatures(self):  # 50 pokedex expected
        response = self.get_request(self.get_url_list_from_basename(self.BASENAME_POKEDEX_CREATURE))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         "L'administrateur doit pouvoir récupérer tous les pokedex creatures.")
        self.assertEqual(len(response.data['results']), 50, "Le total des pokedex récupéré n'est pas bon.")

    # Get one pokedex_creature
    def test_get_one_pokedex_creature(self):
        response = self.get_request(
            self.get_url_detail_from_basename(self.BASENAME_POKEDEX_CREATURE, self.pokedex_creature.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         "L'administrateur doit pouvoir récupérer le pokedex creature")
        self.assertEqual(response.data['legendary'], True,
                         "La valeur du field 'legendary' ne correspond pas avec le résultât attendu.")
