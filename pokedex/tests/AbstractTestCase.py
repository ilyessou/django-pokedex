import json

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient


class AbstractTestCase(TestCase):
    BASENAME_POKEDEX_CREATURE = 'pokedex_creature:pokedex_get_or_list'
    ENDPOINT_POKEMON = 'pokemon:pokemon'
    ENDPOINT_POKEMON_GIVE_XP = 'pokemon:pokemon-give-xp'

    def setUp(self):
        self.admin = User.objects.create(username='admin', password='admin')
        self.client = APIClient()
        self.client.force_authenticate(self.admin)

    # Get the end_point_url for GET/PUT/PATCH/{id}
    def get_url_detail_from_basename(self, basename, args):
        return reverse(basename + '-detail', args=(args,))

    # Get the end_point_url for POST/GET
    def get_url_list_from_basename(self, basename):
        return reverse(basename + '-list')

    # Get the end_point_url for action GET/PUT/PATCH/{id}
    def get_url_action_from_basename(self, basename, args):
        return reverse(basename, args=(args,))

    def get_request(self, url):
        return self.client.get(url, content_type='application/json')

    def post_request(self, url, data={}):
        return self.client.post(url, json.dumps(data), content_type='application/json')

    def patch_request(self, url, data={}):
        return self.client.patch(url, json.dumps(data), content_type='application/json')

    def delete_request(self, url):
        return self.client.delete(url, content_type='application/json')
