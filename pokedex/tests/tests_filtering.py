from pokedex.models import PokedexCreature
from pokedex.tests.AbstractTestCase import AbstractTestCase
from pokedex.tests.generics import create_pokedex_creature


class FilteringTests(AbstractTestCase):

    def setUp(self):
        super().setUp()
        for index in range(50):

            if index < 25:  # Bergmites
                create_pokedex_creature(self.admin, f'Bergmite{index}', type1='Fire', type2='Dragon',
                                        attack=60, defense=30)
            elif 25 < index < 31:  # Goomys
                create_pokedex_creature(self.admin, f'Goomy{index}', type1='Rock', type2='Electric',
                                        attack=90, defense=46)
            else:  # Pikachus
                create_pokedex_creature(self.admin, f'Pikachu{index}', type1='Electric', type2='Electric',
                                        attack=55, defense=40)

        self.pokedex_creature = PokedexCreature.objects.first()

    # Get pokedex creatures with specified filters
    def test_get_pokedex_creatures_with_specific_filters(self):
        """
        This test should ensure that the filters on the urls work perfectly.\n
        The function tests 3 different scenarios:

        * The first one, the return must contain only pokedexes with the name 'Bergmite'.
        * The 2nd one must return all pokedexes with "Type2 = Electric" and "Defense <= 46".
        * The 3rd one must return only pokedexes with the name 'Pikachu'.
        """

        url = self.get_url_list_from_basename(self.BASENAME_POKEDEX_CREATURE)

        # Bergmite filters
        response = self.get_request(url + f"?type1=Fire")  # Expected 25 rows (All Type1 = Fire are Bergmite pokedex)
        self.assertEqual(response.data['count'], 25)
        results_length = len(response.data['results'])
        [self.assertIn('Bergm', response.data['results'][index]['name'], 'Bergmit filters are not working correctly.')
         for index in range(results_length)]

        # Type2 = Electric and Defense <= 46
        response = self.get_request(url + f"?type2=Electric&defense__lte=46")  # Expected 25 rows
        self.assertEqual(response.data['count'], 25,
                         "Le filtre sur les field 'type2' et 'defense__lte' ne marchent pas correctement.")

        # Pikachus filters
        response = self.get_request(url + f"?type2=Electric&defense__lte=46&name__startswith=Pika")  # Expected 20 rows
        results_length = len(response.data['results'])
        self.assertEqual(results_length, 20)
        [self.assertIn('Pika', response.data['results'][index]['name'], 'Pikachus filters are not working correctly.')
         for index in range(results_length)]
