from pokedex.models import PokedexCreature
from pokemon.models import Pokemon


def create_pokedex_creature(created_by, name='pokedex_creature_test', type1='type1_test', type2='type2_test',
                            total=None, hp=None, attack=None, defense=None, sp_atk=None, sp_def=None, speed=None,
                            generation=None, legendary=False):
    return PokedexCreature.objects.create(created_by=created_by, name=name, type1=type1, type2=type2, total=total,
                                          hp=hp, attack=attack, defense=defense, sp_atk=sp_atk, sp_def=sp_def,
                                          speed=speed, generation=generation, legendary=legendary)


def create_pokemon(created_by, pokedex_creature, trainer_id=0, level=1, experience=1, surname=None):
    return Pokemon.objects.create(created_by=created_by, trainer_id=trainer_id, level=level,
                                  experience=experience, pokedex_creature=pokedex_creature,
                                  surname=surname)
