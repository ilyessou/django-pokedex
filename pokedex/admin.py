from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import PokedexCreature


class PokedexCreatureAdmin(ModelAdmin):
    search_fields = ['name', 'type1', 'type2']
    # raw_id_fields = ['']
    list_filter = ['creation_date', 'legendary']
    list_display = ['name', 'type1', 'type1', 'total', 'hp', 'attack']


admin.site.register(PokedexCreature, PokedexCreatureAdmin)
