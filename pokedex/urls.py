from rest_framework import routers

from .apis.GET_LIST_pokedex import PokedexCreatureGetOrList

app_name = 'pokedex_creature'
router = routers.SimpleRouter()

router.register('', PokedexCreatureGetOrList, basename="pokedex_get_or_list")
urlpatterns = router.urls
