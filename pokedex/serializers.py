from rest_framework import serializers

from .models import PokedexCreature


class PokedexCreatureSerializerList(serializers.ModelSerializer):
    class Meta:
        model = PokedexCreature
        fields = ['id', 'name', 'speed', 'legendary']


class PokedexCreatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = PokedexCreature
        fields = '__all__'
