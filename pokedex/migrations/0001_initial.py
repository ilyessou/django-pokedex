# Generated by Django 3.1.3 on 2022-02-20 01:14

from django.conf import settings
from django.db import migrations, models
import django.utils.timezone
import pokedex.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PokedexCreature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('type1', models.CharField(blank=True, max_length=100, null=True)),
                ('type2', models.CharField(blank=True, max_length=100, null=True)),
                ('total', models.IntegerField(blank=True, null=True)),
                ('hp', models.IntegerField(blank=True, null=True)),
                ('attack', models.IntegerField(blank=True, null=True)),
                ('defense', models.IntegerField(blank=True, null=True)),
                ('sp_atk', models.IntegerField(blank=True, null=True)),
                ('sp_def', models.IntegerField(blank=True, null=True)),
                ('speed', models.IntegerField(blank=True, null=True)),
                ('generation', models.IntegerField(blank=True, null=True)),
                ('legendary', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(on_delete=models.SET(pokedex.models.get_sentinel_user), related_name='pokedex_creature_created_by', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
