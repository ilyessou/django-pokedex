from rest_framework.viewsets import ReadOnlyModelViewSet
from url_filter.integrations.drf import DjangoFilterBackend

from common_constants.pagination import Pagination
from common_constants.constants_filters import POKEDEX_CREATURE_FILTERS
from pokedex.models import PokedexCreature
from pokedex.serializers import PokedexCreatureSerializer, PokedexCreatureSerializerList


class PokedexCreatureGetOrList(ReadOnlyModelViewSet):
    queryset = PokedexCreature.objects.order_by('-id')
    filter_fields = POKEDEX_CREATURE_FILTERS
    filter_backends = [DjangoFilterBackend]
    pagination_class = Pagination

    def get_serializer_class(self):
        if self.action == 'list':
            return PokedexCreatureSerializerList
        else:
            return PokedexCreatureSerializer
