DEVELOPER GUIDE  
---

The documentation explains how to run `django_pokedex` on local environment.

> If you us `Pycharm` and you have problems importing plugin:
right click on [django_pokedex](django_pokedex) folder and mark as Root source 

# The Docker stack

We use [docker-compose](https://docs.docker.com/compose/) to define all the required services. Launch it with the following command:

> docker-compose up


Run a shell command in the container
> docker-compose exec web sh

Run migrations (or other commands)
> docker-compose exec web manage.py migrate

## Create environment variables

copy .env.dist to .env file and update with your local settings

## Requirements

    pip install -r requirements.txt

## Available ports
Once all services are launched, some ports are mapped on local computer. You can access with http://localhost:8000.

|  Service | Port  |   
|---|---|
|  Web client | 8000 (8888 Jupyter Notebook)  |
| Postgresql  | 5432  |  
| PgAdmin  | 8083  |

### PGADMIN

* Login: admin@simco.com
* Password: simco

## Jupyter Notebook
Run notebook manually and retrieve given url with token

>    docker-compose exec web python3 manage.py shell_plus --notebook

>To access Notebook once services are up retrieve the given url with working token like:\
`http://127.0.0.1:8888/?token=746aa0edac2d52a8e0772941e1000badaba3df93fe0981d3`

## Create local superuser
>    ./manage.py createsuperuser


## Execution des tests : 
    
>    docker-compose exec web python manage.py test [pokedex.tests](pokedex/tests)

## Details about the django project:
>    We have created two apps :\
        1- [pokedex](pokedex)\
        2- [pokemon](pokemon)\
    Every app has `api` folder which contains the crud of the models.\
> Apis are 100% class based views


## Fill the database with datas:
>   1- Open Jupyter notebook, and go to [Jupyter Folder](Jupyter Folder), and open the script :\
    2- [Script_for_instance_creation_from_excel.ipynb](Jupyter Folder/Script_for_instance_creation_from_excel.ipynb)\
    3- Run the notebook