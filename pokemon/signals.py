from django.db.models.signals import pre_save
from django.dispatch import receiver

from pokemon.models import Pokemon


@receiver(pre_save, sender=Pokemon)
def pre_save_pokemon(sender, instance, **kwargs):
    if instance.pk is None:
        if not instance.surname:  # If we don't give a surname to our pokemon, than he takes the name of his pokedex_creature
            instance.surname = instance.pokedex_creature.name
