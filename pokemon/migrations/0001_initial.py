# Generated by Django 3.1.3 on 2022-02-20 01:49

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import pokedex.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('pokedex', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pokemon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('trainer_id', models.BooleanField(default=False)),
                ('level', models.IntegerField(blank=True, null=True)),
                ('experience', models.IntegerField(blank=True, null=True)),
                ('surname', models.CharField(blank=True, max_length=100, null=True)),
                ('created_by', models.ForeignKey(on_delete=models.SET(pokedex.models.get_sentinel_user), related_name='pokemons', to=settings.AUTH_USER_MODEL)),
                ('pokedex_creature', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='pokedex.pokedexcreature')),
            ],
        ),
    ]
