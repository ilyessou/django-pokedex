from rest_framework import status

from pokedex.tests.AbstractTestCase import AbstractTestCase
from pokedex.tests.generics import create_pokemon, create_pokedex_creature
from pokemon.models import Pokemon


class CrudTests(AbstractTestCase):

    def setUp(self):
        super().setUp()

        for index in range(1, 51):
            pokedex = create_pokedex_creature(self.admin, f'pokedex_creature_test{index}')
            create_pokemon(self.admin, pokedex, level=index)

        self.pokemon = Pokemon.objects.first()

    # Get all pokemons
    def test_get_all_pokemons(self):
        response = self.get_request(self.get_url_list_from_basename(self.ENDPOINT_POKEMON))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         "L'administrateur doit pouvoir récupérer tous les pokemons.")

    # Get one pokemon
    def test_get_one_pokemon(self):
        response = self.get_request(self.get_url_detail_from_basename(self.ENDPOINT_POKEMON, self.pokemon.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         "L'administrateur doit pouvoir récupérer le pokemon.")

    # Post one pokemon
    def test_post_pokemon(self):
        pokedex = create_pokedex_creature(self.admin, 'pokedex_creature_test')
        data = {'pokedex_creature': pokedex.id, 'level': 1, 'experience': 1}
        response = self.post_request(self.get_url_list_from_basename(self.ENDPOINT_POKEMON), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED,
                         "L'administrateur doit pouvoir créer un pokemon")

    # Update a pokemon
    def test_patch_pokemon(self):
        data = {'created_by': self.admin.id, 'level': 5, 'experience': 99}
        response = self.patch_request(self.get_url_detail_from_basename(self.ENDPOINT_POKEMON, self.pokemon.id), data)
        self.assertEqual(response.data['level'], 5, "Le champ 'level' n'a pas été mis à jour.")
        self.assertEqual(response.status_code, status.HTTP_200_OK,
                         "L'administrateur doit pouvoir modifier un pokemon.")

    # Delete a pokemon
    def test_delete_pokemon(self):
        response = self.delete_request(
            self.get_url_detail_from_basename(self.ENDPOINT_POKEMON, self.pokemon.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT,
                         "L'administrateur doit pouvoir supprimer un pokemon.")

    # Test action give_xp
    def test_give_xp_action(self):
        data = {'amount': 502}  # expected +5 lvl and +2 experiences
        response = self.client.post(self.get_url_action_from_basename(self.ENDPOINT_POKEMON_GIVE_XP, self.pokemon.id),
                                    data)
        self.assertEqual(response.data['level'], 6)
        self.assertEqual(response.data['experience'], 3)
