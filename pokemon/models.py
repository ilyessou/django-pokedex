from django.conf import settings
from django.db import models
from django.utils import timezone

from pokedex.models import get_sentinel_user, PokedexCreature


class Pokemon(models.Model):
    creation_date = models.DateTimeField(default=timezone.now)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET(get_sentinel_user),
                                   related_name='pokemons')
    trainer_id = models.BooleanField(default=False)
    level = models.IntegerField(blank=True, null=True)
    experience = models.IntegerField(blank=True, null=True)
    # Fait référence à un ou plusieurs pokedex
    pokedex_creature = models.ForeignKey(PokedexCreature, on_delete=models.SET_NULL, null=True, blank=False)
    surname = models.CharField(blank=True, null=True, max_length=100)
