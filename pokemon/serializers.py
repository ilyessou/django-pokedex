from rest_framework import serializers

from .models import Pokemon


class PokemonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pokemon
        fields = '__all__'
        extra_kwargs = {'created_by': {'required': False}}

    def create(self, validated_data):
        validated_data['created_by'] = self.context['request'].user
        return super(PokemonSerializer, self).create(validated_data)
