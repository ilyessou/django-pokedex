from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models import Pokemon


class PokemonAdmin(ModelAdmin):
    search_fields = ['surname', 'level', 'experience', 'pokedex_creature', 'trainer_id']
    # raw_id_fields = ['']
    list_filter = ['trainer_id', 'level']
    list_display = ['surname', 'level', 'experience', 'pokedex_creature', 'trainer_id']


admin.site.register(Pokemon, PokemonAdmin)
