from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from url_filter.integrations.drf import DjangoFilterBackend

from common_constants.pagination import Pagination
from common_constants.constants_filters import POKEMON_FILTERS
from pokemon.models import Pokemon
from pokemon.serializers import PokemonSerializer


class PokemonCrud(ModelViewSet):
    queryset = Pokemon.objects.order_by('-id')
    filter_fields = POKEMON_FILTERS
    serializer_class = PokemonSerializer
    filter_backends = [DjangoFilterBackend]
    pagination_class = Pagination

    @action(methods=['post'], detail=True)
    def give_xp(self, request, pk=None):
        pokemon = Pokemon.objects.get(id=pk)
        if 'amount' in request.data:
            XP_100 = 100
            amount = int(request.data['amount'])
            if pokemon.experience + amount >= XP_100:
                total_exp = pokemon.experience + amount
                pokemon.experience = total_exp % XP_100  # Example 249 % 100 = 49
                pokemon.level = pokemon.level + int(total_exp / XP_100)  # Example 249 / 100 = + 2 levels
            else:
                pokemon.experience += amount
            pokemon.save()
            serialized_pokemon = PokemonSerializer(pokemon, many=False)
            return Response(serialized_pokemon.data)
        else:
            return Response("'amount' field is required to give xp to the pokemon.")
