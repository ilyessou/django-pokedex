from rest_framework import routers

from .apis.CRUD_pokemon import PokemonCrud

app_name = 'pokemon'
router = routers.SimpleRouter()

router.register('', PokemonCrud, basename="pokemon")
urlpatterns = router.urls
