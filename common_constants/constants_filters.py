from pokedex.models import PokedexCreature
from pokemon.models import Pokemon

POKEDEX_CREATURE_FILTERS = [f.name for f in PokedexCreature._meta.get_fields()]

POKEMON_FILTERS = [f.name for f in Pokemon._meta.get_fields()]
